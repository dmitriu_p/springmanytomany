/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.springhibmanytomany.controller;

import com.mycompany.springhibmanytomany.model.Student;
import com.mycompany.springhibmanytomany.service.StudentService;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import static org.mockito.Mockito.atLeastOnce;

import org.springframework.context.MessageSource;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.testng.Assert;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 *
 * @author Дмитрий
 */
public class AppControllerTest {
    
    @Mock
    StudentService service;
     
    @Mock
    MessageSource message;
     
    @InjectMocks
    AppController appController;
     
    @Spy
    List<Student> students = new ArrayList<Student>();
 
    @Spy
    ModelMap model;
     
    @Mock
    BindingResult result;
     
    @BeforeClass
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        students = getStudentList();
    }
    
    /*    @Test
    public void listStudents(){
    when(service.findAllStudents()).thenReturn(students);
    Assert.assertEquals(appController.listUsers(model), "studentlist");
    Assert.assertEquals(model.get("students"), students);
    verify(service, atLeastOnce()).findAllStudents();
    }*/
    
    /*        @Test
    public void newStudent(){
    Assert.assertEquals(appController.newUser(model), "registration");
    Assert.assertNotNull(model.get("student"));
    Assert.assertFalse((Boolean)model.get("edit"));
    Assert.assertEquals(((Student)model.get("student")).getId(), 0);
    }*/
    
    /*    @Test
    public void saveStudent(){
    doNothing().when(service).saveStudent(any(Student.class));
    Assert.assertEquals(appController.saveUser(students.get(0), result, model), "registrationsuccess");
    Assert.assertEquals(model.get("success"), "User Axel Vort registered successfully");
    }*/
    
    /*    @Test
    public void editStudent(){
    Student emp = students.get(1);
    when(service.findById(anyInt())).thenReturn(emp);
    Assert.assertEquals(appController.editUser(anyInt(), model), "registration");
    Assert.assertNotNull(model.get("student"));
    Assert.assertTrue((Boolean)model.get("edit"));
    Assert.assertEquals(((Student)model.get("student")).getId(), 1);
    }*/
    
     @Test
    public void deleteStudent(){
        doNothing().when(service).deleteStudent(anyInt());
        Assert.assertEquals(appController.deleteUser(123), "redirect:/list");
    }
    
    public List<Student> getStudentList(){
        Student e1 = new Student();
        e1.setId(7);
        e1.setName("Axel");
        e1.setLastName("Vort");
         
        Student e2 = new Student();
        e2.setId(8);
        e2.setName("Jeremy");
        e2.setLastName("Opel");
  
        students.add(e1);
        students.add(e2);
        return students;
    }
    
}
