/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.springhibmanytomany.dao;

import com.mycompany.springhibmanytomany.model.Student;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 *
 * @author Дмитрий
 */
public class StudentDaoImpTest extends EntityDaoImplTest {
    
    @Autowired
    StudentDao studentDao;
 
    @Override
    protected IDataSet getDataSet() throws Exception{
        IDataSet dataSet = new FlatXmlDataSet(this.getClass().getClassLoader().getResourceAsStream("Student.xml"));
        return dataSet;
    } 
    
    @Test
    public void findById(){
        Assert.assertNotNull(studentDao.findById(1));
        Assert.assertNull(studentDao.findById(3));
    }
    
    @Test
    public void saveStudent(){
        studentDao.save(getStudent());
        Assert.assertEquals(studentDao.findAllStudents().size(), 3);
    }
    
    @Test
    public void deleteStudent(){
        studentDao.delete(1);
        Assert.assertEquals(studentDao.findAllStudents().size(), 1);
    }
    
    @Test
    public void findAllStudents(){
        Assert.assertEquals(studentDao.findAllStudents().size(), 2);
    }
    
    public Student getStudent(){
        Student student = new Student();
        student.setName("Kara");
        student.setLastName("Ora");
        return student;
    } 
}
