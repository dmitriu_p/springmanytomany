/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.springhibmanytomany.service;

import com.mycompany.springhibmanytomany.dao.StudentDao;
import com.mycompany.springhibmanytomany.model.Student;
import java.util.ArrayList;
import java.util.List;
import org.mockito.InjectMocks;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import org.mockito.Mock;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 *
 * @author Дмитрий
 */
public class StudentServiceImplTest {
    
    @Mock
    StudentDao dao;
     
    @InjectMocks
    StudentServiceImpl studentService;
    
    @Spy
    List<Student> students = new ArrayList<Student>();
     
    @BeforeClass
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        students = getStudentList();
    }
    
    @Test
    public void findById(){
        Student emp = students.get(0);
        when(dao.findById(anyInt())).thenReturn(emp);
        Assert.assertEquals(studentService.findById(emp.getId()),emp);
    }
    
    @Test
    public void saveStudent(){
        doNothing().when(dao).save(any(Student.class));
        studentService.saveStudent(any(Student.class));
        verify(dao, atLeastOnce()).save(any(Student.class));
    }
    
     @Test
    public void updateStudent(){
        Student emp = students.get(0);
        when(dao.findById(anyInt())).thenReturn(emp);
        studentService.updateStudent(emp);
        verify(dao, atLeastOnce()).findById(anyInt());
    }
    
    @Test
    public void deleteStudent(){
        doNothing().when(dao).delete(anyInt());
        studentService.deleteStudent(anyInt());
        verify(dao, atLeastOnce()).delete(anyInt());
    }
    
    @Test
    public void findAllStudents(){
        when(dao.findAllStudents()).thenReturn(students);
        Assert.assertEquals(studentService.findAllStudents(), students);
    }
    
    public List<Student> getStudentList(){
        Student e1 = new Student();
        e1.setId(7);
        e1.setName("Axel");
        e1.setLastName("Vort");
         
        Student e2 = new Student();
        e2.setId(8);
        e2.setName("Jeremy");
        e2.setLastName("Opel");
  
        students.add(e1);
        students.add(e2);
        return students;
    }
    
}
