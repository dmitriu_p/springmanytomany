<%-- 
    Document   : registrationsuccess
    Created on : 29.02.2016, 15:37:39
    Author     : Дмитрий
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet"></link>
        <link href="<c:url value='/static/css/app.css' />" rel="stylesheet"></link>
        <title>Success Page</title>
    </head>
    <body>
    <div class="generic-container">
        <div class="alert alert-success lead">
            ${success}
        </div>
     
        <span class="well floatRight">
        Go to <a href="<c:url value='/list' />">Users List</a>
        </span>
</div>
</body>
</html>
