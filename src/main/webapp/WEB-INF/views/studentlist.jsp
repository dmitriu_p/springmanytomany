<%-- 
    Document   : userlist
    Created on : 29.02.2016, 15:28:04
    Author     : Дмитрий
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet"></link>
        <link href="<c:url value='/static/css/app.css' />" rel="stylesheet"></link>
        
        <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
        <spring:url value='/static/resources/viewStudent.js'
	var="jqueryJs" />
        <script src="${jqueryJs}"></script>
        <title>List</title>
    </head>
    <body>
        <div id="mask" style="display: none;"></div>
	<div id="popup" style="display: none;">
		<div class="container" id="insertHere">
			<div class="span-1 append-23 last">
				<p><a href="#" onclick="closePopup();">Close</a></p>
			</div>
		</div>
	</div>
    <div class="generic-container">
        <div class="panel panel-default">
              <!-- Default panel contents -->
            <div class="panel-heading"><span class="lead">List of Users </span></div>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Last Name</th>
                        <th>Subject</th>
                        <th width="100"></th>
                        <th width="100"></th>
                    </tr>
                </thead>
                <tbody>
                <form:form modelAttribute="student" id="target"  action="/more-user" method="post">    
                <c:forEach items="${students}" var="student">
                    <tr>
                        <td>${student.name}</td>
                        <td>${student.lastName}</td>
                        <td><c:forEach items="${student.subjectProfiles}" var="subject">
                                ${subject.title}<br>  
                            </c:forEach></td>
                        <td><a href="<c:url value='/edit-user-${student.id}' />" class="btn btn-success custom-width">edit</a></td>
                        <td><a href="<c:url value='/delete-user-${student.id}' />" class="btn btn-danger custom-width">delete</a></td>
                        <!-- <td><input  class="btn btn-success custom-width" type="submit" name="action" value="more" accesskey="A" /></td> -->
                    </tr>
                </c:forEach>
                    </form:form>
                </tbody>
            </table>
        </div>
        <div class="well">
            <a href="<c:url value='/newuser' />">Add New User</a>
        </div>
    </div>
</body>
</html>
