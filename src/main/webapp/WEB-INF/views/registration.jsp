<%-- 
    Document   : registration
    Created on : 29.02.2016, 15:30:08
    Author     : Дмитрий
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet"></link>
        <link href="<c:url value='/static/css/app.css' />" rel="stylesheet"></link>
        
        <spring:url value="/js/jquery.js" var="jqueryUrl" />
	<script src="${jqueryUrl}"></script>
        <title>Create Page</title>
    </head>
    <body>
 
        
    <div class="generic-container">
        <spring:url value="/newuser" var="formUrl" />
        <spring:url value="/newuser.json" var="formJsonUrl" />
    <div class="well lead">User Registration Form</div>
    <form:form  modelAttribute="student" action="${formUrl}" class="form-horizontal" id="add-student-form">
        <form:input type="hidden" path="id" id="id"/>
         
        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable" for="name">First Name</label>
                <div class="col-md-7">
                    <form:input type="text" path="name" id="name" class="form-control input-sm"/>
                    <div class="has-error">
                        <form:errors path="name" class="help-inline"/>
                        <span class="help-inline"><form:errors path="name"/></span>
                    </div>
                </div>
            </div>
        </div>
 
        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable" for="lastName">Last Name</label>
                <div class="col-md-7">
                    <form:input type="text" path="lastName" id="lastName" class="form-control input-sm" />
                    <div class="has-error">
                        <form:errors path="lastName" class="help-inline"/>
                        <span class="help-inline"><form:errors path="lastName"/></span>
                    </div>
                </div>
            </div>
        </div>
 
        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable" for="subjectProfiles">Roles</label>
                <div class="col-md-7">
                    <form:select path="subjectProfiles" items="${roles}" multiple="true" itemValue="id" itemLabel="title" class="form-control input-sm" />
                    <div class="has-error">
                        <form:errors path="subjectProfiles" class="help-inline"/>
                    </div>
                </div>
            </div>
        </div>
 
        <div class="row">
            <div class="form-actions floatRight">
                <c:choose>
                    <c:when test="${edit}">
                        <input type="submit" value="Update" class="btn btn-primary btn-sm"/> or <a href="<c:url value='/list' />">Cancel</a>
                    </c:when>
                    <c:otherwise>
                        <input type="submit" value="Register" class="btn btn-primary btn-sm"/> or <a href="<c:url value='/list' />">Cancel</a>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </form:form>
    </div>
        
 <script type="text/javascript">
		<![CDATA[function collectFormData(fields) {
				var data = {};
				for (var i = 0; i < fields.length; i++) {
					var $item = $(fields[i]);
					data[$item.attr('name')] = $item.val();
				}
				return data;
			}
				
			$(document).ready(function() {
				var $form = $('#add-student-form');
				$form.bind('submit', function(e) {
					// Ajax validation
					var $inputs = $form.find('input');
					var data = collectFormData($inputs);
					
					$.post('${formJsonUrl}', data, function(response) {
						$form.find('.form-group col-md-12').removeClass('error');
						$form.find('.help-inline').empty();
						$form.find('.alert').remove();
						
						if (response.status == 'FAIL') {
							for (var i = 0; i < response.errorMessageList.length; i++) {
								var item = response.errorMessageList[i];
								var $controlGroup = $('#' + item.fieldName + 'ControlGroup');
								$controlGroup.addClass('error');
								$controlGroup.find('.has-error').html(item.message);
							}
						} else {
							$form.unbind('submit');
							$form.submit();
						}
					}, 'json');
					
					e.preventDefault();
					return false;
				});
			});
			]]>
		</script>    
</body>
</html>
