/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.springhibmanytomany.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author Дмитрий
 */
@Entity
@Table(name = "student")

public class Student implements Serializable {
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id", unique=true, nullable=false)
    private Integer id;
    
    @NotEmpty
    @Column(name="name", nullable=false)
    private String name;
    
    @NotEmpty
    @Column(name="last_name", nullable=false)
    private String lastName;
    
    @NotEmpty
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "student_subject", 
             joinColumns = { @JoinColumn(name = "student_id") }, 
             inverseJoinColumns = { @JoinColumn(name = "subject_id") })
    
    private Set<Subject> subjectProfiles = new HashSet<Subject>();
    
    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    
    public Set<Subject> getSubjectProfiles() {
        return subjectProfiles;
    }
 
    public void setSubjectProfiles(Set<Subject> subjectProfiles) {
        this.subjectProfiles = subjectProfiles;
    }
    
    
     @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }
 
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof Student))
            return false;
        Student other = (Student) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }
       
    @Override
    public String toString() {
        return "номер id " + id  + " имя студента " + name  + " фамилия студента " + lastName + "\n";
    }   
}
