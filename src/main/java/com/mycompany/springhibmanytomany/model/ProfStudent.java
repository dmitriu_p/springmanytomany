/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.springhibmanytomany.model;

/**
 *
 * @author Дмитрий
 */
public enum ProfStudent {
    Physics("Physics"),
    Mathematics("Mathematics"),
    Philosophy("Philosophy");
     
    String subjectTitle;
     
    private ProfStudent(String subjectTitle){
        this.subjectTitle = subjectTitle;
    }
     
    public String getSubjectTitle(){
        return subjectTitle;
    }
    
}
