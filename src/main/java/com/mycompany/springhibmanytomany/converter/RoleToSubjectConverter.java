/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.springhibmanytomany.converter;

import com.mycompany.springhibmanytomany.model.Subject;
import com.mycompany.springhibmanytomany.service.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 *
 * @author Дмитрий
 */

@Component
public class RoleToSubjectConverter implements Converter<Object, Subject> {
    
    @Autowired
    SubjectService subjectService;
 

    public Subject convert(Object element) {
        Integer id = Integer.parseInt((String)element);
        Subject profile = subjectService.findById(id);
        System.out.println("Profile : "+profile);
        return profile;
    }
    
    
}
