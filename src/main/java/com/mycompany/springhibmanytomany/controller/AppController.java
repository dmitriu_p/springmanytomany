/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.springhibmanytomany.controller;

import com.mycompany.springhibmanytomany.model.ErrorMessage;
import com.mycompany.springhibmanytomany.model.Student;
import com.mycompany.springhibmanytomany.model.Subject;
import com.mycompany.springhibmanytomany.model.ValidationResponse;
import com.mycompany.springhibmanytomany.service.StudentService;
import com.mycompany.springhibmanytomany.service.SubjectService;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

/**
 *
 * @author Дмитрий
 */

@Controller
@RequestMapping("/")
@SessionAttributes("roles")
public class AppController {
    
    @Autowired
    StudentService studentService;
     
    @Autowired
    SubjectService subjectService;
     
     
    @Autowired
    MessageSource messageSource;
 
    @RequestMapping(value = { "/", "/list" }, method = RequestMethod.GET)
    public String listUsers(ModelMap model) {
 
        List<Student> students = studentService.findAllStudents();
        model.addAttribute("students", students);
        return "studentlist";
    }
 
    @RequestMapping(value = { "/newuser" }, method = RequestMethod.GET)
    public String newUser(@Valid ModelMap model) {
        Student student = new Student();
        model.addAttribute("student", student);
        model.addAttribute("edit", false);
        return "registration";
    }
 
    @RequestMapping(value = { "/newuser" }, method = RequestMethod.POST)
    public String saveUser(@ModelAttribute(value="student") @Valid Student student, BindingResult result,
            ModelMap model) {
 
        /*if (result.hasErrors()) {
        return "registration";
        }*/
         
        studentService.saveStudent(student);
 
        model.addAttribute("success", "Student " + student.getName() + " "+ student.getLastName() + " registered successfully");
        return "registrationsuccess";
    }

    @RequestMapping(value = { "/more-user" }, method = RequestMethod.POST)
    public @ResponseBody
	Student moreStudent( ModelMap model) {
        Student student = studentService.findById(1);
        model.addAttribute("student", student);
        return student;
    }
        
        @RequestMapping(value="/newuser.json",method=RequestMethod.POST)
	public @ResponseBody ValidationResponse processFormAjaxJson(Model model, @ModelAttribute(value="user") @Valid Student user, BindingResult result ){
		ValidationResponse res = new ValidationResponse();
		if(result.hasErrors()){
			res.setStatus("FAIL");
			List<FieldError> allErrors = result.getFieldErrors();
			List<ErrorMessage> errorMesages = new ArrayList<ErrorMessage>();
			for (FieldError objectError : allErrors) {
				errorMesages.add(new ErrorMessage(objectError.getField(), objectError.getField() + "  " + objectError.getDefaultMessage()));
			}
			res.setErrorMessageList(errorMesages);

		}else{
			res.setStatus("SUCCESS");
		}
		
		return res;
	}
    
    @RequestMapping(value = { "/edit-user-{id}" }, method = RequestMethod.GET)
    public String editUser(@PathVariable Integer id, ModelMap model) {
        Student student = studentService.findById(id);
        model.addAttribute("student", student);
        model.addAttribute("edit", true);
        return "registration";
    }
     
    @RequestMapping(value = { "/edit-user-{id}" }, method = RequestMethod.POST)
    public String updateUser(@Valid Student student, BindingResult result,
            ModelMap model) {
 
        if (result.hasErrors()) {
            return "registration";
        }
 
        studentService.updateStudent(student);
 
        model.addAttribute("success", "User " + student.getName() + " "+ student.getLastName() + " updated successfully");
        return "registrationsuccess";
    }
 
    @RequestMapping(value = { "/delete-user-{id}" }, method = RequestMethod.GET)
    public String deleteUser(@PathVariable Integer id) {
        studentService.deleteStudent(id);
        return "redirect:/list";
    }

    @ModelAttribute("roles")
    public List<Subject> initializeProfiles() {
        return subjectService.findAll();
    }
 

    
}
