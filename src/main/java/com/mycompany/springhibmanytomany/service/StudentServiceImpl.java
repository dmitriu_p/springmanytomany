/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.springhibmanytomany.service;

import com.mycompany.springhibmanytomany.dao.StudentDao;
import com.mycompany.springhibmanytomany.model.Student;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Дмитрий
 */

@Service("studentService")
@Transactional
public class StudentServiceImpl implements StudentService {
   
    @Autowired
    private StudentDao dao;
 
    public  Student findById(int id) {
        return dao.findById(id);
    }
 
    public void saveStudent(Student student) {
        dao.save(student);
    }
 
    /*
     * Since the method is running with Transaction, No need to call hibernate update explicitly.
     * Just fetch the entity from db and update it with proper values within transaction.
     * It will be updated in db once transaction ends. 
     */
    public void updateStudent(Student student) {
        Student entity = dao.findById(student.getId());
        if(entity!=null){
            entity.setName(student.getName());
            entity.setLastName(student.getLastName());
            entity.setSubjectProfiles(student.getSubjectProfiles());
        }
    }
 
     
    public void deleteStudent(int id) {
        dao.delete(id);
    }
 
    public List<Student> findAllStudents() {
        return dao.findAllStudents();
    }
    
}
