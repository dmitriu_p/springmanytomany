/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.springhibmanytomany.service;


import com.mycompany.springhibmanytomany.dao.SubjectDao;
import com.mycompany.springhibmanytomany.model.Subject;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Дмитрий
 */

@Service("subjectService")
@Transactional
public class SubjectServiceImpl implements SubjectService {
    
    @Autowired
    SubjectDao dao;
     
    public  Subject findById(int id) {
        return dao.findById(id);
    }
 
    public  Subject findByType(String title){
        return dao.findByType(title);
    }
 
    public List< Subject> findAll() {
        return dao.findAll();
    }
    
}
