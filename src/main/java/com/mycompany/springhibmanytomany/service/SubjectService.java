/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.springhibmanytomany.service;

import com.mycompany.springhibmanytomany.model.Subject;
import java.util.List;

/**
 *
 * @author Дмитрий
 */
public interface SubjectService {
    
    Subject findById(int id);
 
    Subject findByType(String title);
     
    List<Subject> findAll();
    
}
