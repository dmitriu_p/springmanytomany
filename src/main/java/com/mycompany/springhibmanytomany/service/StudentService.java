/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.springhibmanytomany.service;

import com.mycompany.springhibmanytomany.model.Student;
import java.util.List;

/**
 *
 * @author Дмитрий
 */
public interface StudentService {
    
    Student findById(int id);
     
    void saveStudent(Student student);
     
    void updateStudent(Student student);
     
    void deleteStudent(int id);
 
    List<Student> findAllStudents(); 
    
}
