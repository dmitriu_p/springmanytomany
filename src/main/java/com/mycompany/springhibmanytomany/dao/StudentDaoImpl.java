/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.springhibmanytomany.dao;

import com.mycompany.springhibmanytomany.model.Student;
import com.mycompany.springhibmanytomany.model.Subject;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Дмитрий
 */

@Repository("studentDao")
public class StudentDaoImpl extends AbstractDao<Integer, Student> implements StudentDao {
    
    public Student findById(int id) {
        Student student = getByKey(id);
        if(student!=null){
            Hibernate.initialize(student.getSubjectProfiles());
        }
        return student;
    }
 
    @SuppressWarnings("unchecked")
    public List<Student> findAllStudents() {
        Criteria criteria = createEntityCriteria().addOrder(Order.asc("name"));
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);//To avoid duplicates.
        List<Student> users = (List<Student>) criteria.list();

        return users;
    }
 
    public void save(Student student) {
        persist(student);
    }
 
    public void delete(int id) {
        Criteria crit = createEntityCriteria();
        crit.add(Restrictions.eq("id", id));
        Student student = (Student)crit.uniqueResult();
        delete(student);
    }
    
}
