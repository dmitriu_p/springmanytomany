/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.springhibmanytomany.dao;

import com.mycompany.springhibmanytomany.model.Student;
import java.util.List;

/**
 *
 * @author Дмитрий
 */
public interface StudentDao {
    
    Student findById(int id);
     
    void save(Student student);
     
    void delete(int id);
     
    List<Student> findAllStudents();
    
}
