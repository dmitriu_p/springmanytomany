/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.springhibmanytomany.dao;

import com.mycompany.springhibmanytomany.model.Subject;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Дмитрий
 */

@Repository("subjectDao")
public class SubjectDaoImpl extends AbstractDao<Integer, Subject>implements SubjectDao{
    
    public Subject findById(int id) {
        return getByKey(id);
    }
 
    public Subject findByType(String title) {
        Criteria crit = createEntityCriteria();
        crit.add(Restrictions.eq("title", title));
        return (Subject) crit.uniqueResult();
    }
     
    @SuppressWarnings("unchecked")
    public List<Subject> findAll(){
        Criteria crit = createEntityCriteria();
        crit.addOrder(Order.asc("title"));
        return (List<Subject>)crit.list();
    }
}
